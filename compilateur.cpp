//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur" 


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL 	{EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD 	{ADD, SUB, OR, WTFA};
enum OPMUL 	{MUL, DIV, MOD, AND, WTFM};
enum TYPE	{INT, BOOL, DOUBLE, CHAR};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement::
// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement | "FOR" AssignementStatement "DOWNTO" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
	
		
TYPE Identifier(void){
	if(!IsDeclared(lexer->YYText()))
	{
		cerr << "Erreur : variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	TYPE t = DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return t;
}

TYPE Number(void){
	bool is_a_decimal=false;
	double d;					// 64-bit float
	unsigned int *i;			// pointer to a 32 bit unsigned int 
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){
		// Floating point constant number
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; // i points to the const double
		//cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to : 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{ // Integer Constant
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INT;
	}

}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT)
	{
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex(); 
	}
	else if (current==NUMBER)
	{
		type = Number();
	}
	else if(current==ID)
	{
		type = Identifier();
	}
	else if(current==CHARCONST)
	{
		type = CharConst();
	}			
	else
	{
		Error("'(' ou chiffre ou lettre attendue");
	}
	return type;	
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void)
{
	TYPE type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type2!=type1)
			Error("types incompatibles dans l'expression");
		switch(mulop){
			case AND:
				if(type2!=BOOL)
					Error("type non booléen pour l'opérateur AND");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INT&&type2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(type2==INT){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(type2!=INT&&type2!=DOUBLE)
					Error("type non numérique pour la division");
				if(type2==INT){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(type2!=INT)
					Error("type non entier pour le modulo");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}


// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE type = Term();
	TYPE type2;
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		if(type2!=type)
		{
			Error("Les deux terms ne sont pas de types compatibles");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type;
}

TYPE Type(void){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOL")==0)
	{
		current=(TOKEN) lexer->yylex();
		return BOOL;
	}	
	else if(strcmp(lexer->YYText(),"INT")==0)
	{
		current=(TOKEN) lexer->yylex();
		return INT;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0)
	{
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0)
	{
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else
		Error("type inconnu");	
}

// Declaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> idents;
	TYPE type;
	if(current!=ID)
		Error("Un identificater était attendu");
	idents.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	type=Type();
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it){
	    switch(type){
			case BOOL:
			case INT:
				cout << *it << ":\t.quad 0"<<endl;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0"<<endl;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0"<<endl;
				break;
			default:
				Error("type inconnu.");
		};
		DeclaredVariables[*it]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN) lexer->yylex();
}


// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	int myTag = ++TagNumber;
	OPREL oprel;
	TYPE type = SimpleExpression();
	TYPE type2;
	if(current==RELOP){
		oprel=RelationalOperator();
		type2 = SimpleExpression();
		if(type2!=type)
		{
			Error("Les deux expressions ne sont pas de types compatibles");
		}
		if(type!=DOUBLE)
		{
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else
		{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}

		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<myTag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<myTag<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<myTag<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<myTag<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<myTag<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<myTag<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<myTag<<endl;
		cout << "Vrai"<<myTag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<myTag<<":"<<endl;
		return BOOL;
	}
	return type;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	TYPE type, type2;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2 = Expression();
	if(type2!=type)
	{
		cerr<<"Type variable "<<type<<endl;
		cerr<<"Type Expression "<<type2<<endl;
		Error("types incompatibles dans l'affectation");
	}
	if(type==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else
	{	
		cout << "\tpop "<<variable<<endl;
	}
	return variable;
}

void IfStatement();
void WhileStatement();
void ForStatement();
void CaseStatement();
void DoWhileStatement();
void BlockStatement();
void Display();

// Statement := AssignementStatement
void Statement(void){
	if(current==KEYWORD)
	{
		if(current==ID){AssignementStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "IF")==0){IfStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "WHILE")==0){WhileStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "FOR")==0){ForStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "BEGIN")==0){BlockStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "DISPLAY")==0){Display();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "CASE")==0){CaseStatement();}
		else if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0){DoWhileStatement();}
		else
			Error("mot clé inconnu");
	}
	else
	{
		if(current==ID)
			AssignementStatement();
		else
			Error("instruction attendue");
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();		
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	int myTag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "IF")==0)
    {
        current=(TOKEN) lexer->yylex();
        TYPE type = Expression();
        if(type!=BOOL)
		{
			Error("Le type de la condition du if n'est pas un bool");
		}
        cout << "\tpop %rax"<<endl;
        cout << "\tmovq $0, %rbx"<<endl;
        cout << "\tcmpq %rax, %rbx"<<endl;
        cout << "\tje ELSE"<<myTag<<endl;
         
        if(current==KEYWORD && strcmp(lexer->YYText(), "THEN")==0) 
        {
            current=(TOKEN) lexer->yylex();
            Statement();
            
            cout << "\tjmp suite"<<myTag<<endl;
            cout << "ELSE"<<myTag<<":"<< endl;
        
            if(current==KEYWORD && strcmp(lexer->YYText(), "ELSE")==0)
            {
                current=(TOKEN) lexer->yylex();
                Statement();
            }
        }
        else{Error("THEN attendu");}            
    }
    else{Error("IF attendu");}
    
    cout << "suite"<<myTag<<":"<<endl;
}

//WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void){
	int myTag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "WHILE")==0)
	{
		current=(TOKEN) lexer->yylex();
		cout << "while"<<myTag<<":"<<endl;
        TYPE type = Expression();
        if(type!=BOOL)
		{
			Error("Le type de la condition du while n'est pas un bool");
		}
        cout << "\tpop %rax"<<endl;
        cout << "\tmovq $0, %rbx"<<endl;
        cout << "\tcmpq %rax, %rbx"<<endl;
        cout << "\tje suite"<<myTag<<endl;
        
        if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
        {
			current=(TOKEN) lexer->yylex();
			Statement();
			
			cout << "\tjmp while"<<myTag<<endl;
		}
		else{Error("DO attendu");}
	}
	else{Error("WHILE attendu");}
	
	cout << "suite"<<myTag<<":"<<endl;
}

//ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
void ForStatement(void)
{
	int myTag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "FOR")==0)
	{
		current=(TOKEN) lexer->yylex();						//trouve le prochain TOKEN
		string variable = AssignementStatement();			//on a fait en sorte que AssignementStatement retourn sa variable
		cout << "for"<<myTag<<":"<<endl;                    //flag pour le retour en fin de DO
		cout << "\tmovq $1, %r8"<<endl;						//initialise le step a 1 au cas ou il n'y en ai pas
		
        if(current==KEYWORD && strcmp(lexer->YYText(), "TO")==0)
        {
			current=(TOKEN) lexer->yylex();
			Expression();
			
			cout << "\tpop %rbx"<<endl;							//pop le resultat de expression
			cout << "\tmovq $0, %rax"<<endl;	           	 	//place 0 dans rax (car si le resultat de la condition for est faux c'est 0 aussi)
			cout << "\tcmpq %rax, %rbx"<<endl;					//compare rbx et rax (rbx-rax)
			cout << "\tja suite"<<myTag<<endl;					//jump if above (si le resultat de b-a est négatif), renvoi au flag suite
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "STEP")==0)
			{
				current=(TOKEN) lexer->yylex();
				Number();
				cout << "\tpop %r8"<<endl;						//extrait dans r8 le nombre apres STEP
			}
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout << "\taddq %r8, "<<variable<<endl;			//le i + step (step = 1 par defaut)
				cout << "\tjmp for"<<myTag<<endl;				//retourne au flag début de for
			}
			else{Error("DO attendu");}
		}
		
        else if(current==KEYWORD && strcmp(lexer->YYText(), "DOWNTO")==0)
        {
			current=(TOKEN) lexer->yylex();
			Expression();
			
			cout << "\tpop %rbx"<<endl;						    //pop le resultat de expression
			cout << "\tmovq $0, %rax"<<endl;	                //place 0 dans rax (car si le resultat de la condition for est faux c'est 0 aussi)
			cout << "\tcmpq %rax, %rbx"<<endl;				    //compare rbx et rax (rbx-rax)
			cout << "\tja suite"<<myTag<<endl;				    //jump if below (si le resultat de b-a est négatif), renvoi au flag suite
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "STEP")==0)
			{
				current=(TOKEN) lexer->yylex();
				Number();
				cout << "\tpop %r8"<<endl;						//extrait dans r8 le nombre apres STEP
			}
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout << "\tsubq %r8, "<<variable<<endl;			//le i - step (step = 1 par defaut)
				cout << "\tjmp for"<<myTag<<endl;			    //retourne au flag début de for
			}
			else{Error("DO attendu");}
		}
		else{Error("DOWNTO ou TO attendu");}
	}
	else{Error("FOR attendu");}
	
	cout << "suite"<<myTag<<":"<<endl;						//le flag suite qui permet de finir le for quand on arrive au bout du "i++"
}

void CaseStatement(void){
    TYPE type, type2;
    unsigned long tagNum=++TagNumber;
    unsigned long tagNum2=++TagNumber;
    unsigned long tagNumSuiv=TagNumber;
    
    if (strcmp(lexer->YYText(), "CASE") !=0)			//verification de la presence du keyword CASE
    {
		Error("CASE attendu");
    }
    
    cout << "SWITCHCASE"<< tagNum << " :" << endl;		//placement du flag SWITCHCASE
    current=(TOKEN) lexer->yylex();
    type = SimpleExpression();							//type de i
    
    if (type != INT)									//verif si i=INT
        Error("INT attendu");
    if (strcmp(lexer->YYText(), "OF") !=0)				//verification de la presence du keyword OF
        Error("OF attendu");
    
    cout << "OF"<< tagNum << " :" << endl;				//placement du flag OF
    current=(TOKEN) lexer->yylex();
    cout << "\tpop %rcx" << endl;						//pop i dans %rcx
    
    while (strcmp(lexer->YYText(), "ELSE") !=0)			//fait tant qu'on ne rencontre pas le keyword ELSE
    {
        cout << "CASE"<< tagNum2 << " :" << endl;		//placement du flag CASE n°tagNum2
        cout << "\tmovq %rcx, %rax" << endl;			//met rcx dans rax
        type2 = SimpleExpression();						//type du tagNum2
        
        if (type2 != type)								//verif si type tagNum2 = type i
            Error("type non identique (fonction CASE)");
        if (strcmp(lexer->YYText(), ":") !=0)			//verification de la presence du keyword ':'
            Error(": attendu");
        tagNumSuiv=++TagNumber;
        tagNum2=tagNumSuiv;
        cout << "\tpop %rbx" << endl;					//met tagNum2 dans %rbx
        cout << "\tcmpq %rax, %rbx" << endl;			//compare %rax et %rbx
        cout << "\tjne CASE" << tagNumSuiv << endl;		//si pas egal saute au cas tagNumSuiv
        current=(TOKEN) lexer->yylex();
        Statement();
        cout << "\tjmp CASEEND" << tagNum << endl;		//saute a la fin du cas
    }
    
    cout << "CASE"<< tagNum2 << " :" << endl;
    current=(TOKEN) lexer->yylex();
    Statement();
    cout << "CASEEND"<< tagNum << " :" << endl;
}

//WhileStatement := "DO" Statement "WHILE" Expression 
void DoWhileStatement(void){
    TYPE type;
    unsigned long tagNum=++TagNumber;
    if (current==KEYWORD && strcmp(lexer->YYText(), "DO")!=0)
    {
	    Error("DO attendu");
    }
    current=(TOKEN) lexer->yylex();
    cout << "\tjmp WHILE" << tagNum << endl;
    cout << "DO" << tagNum << " :" << endl;
    Statement();
    //rajouter begin (pour plusieurs instructions)
    if (strcmp(lexer->YYText(), "WHILE") !=0)
    {
		Error("WHILE attendu");
    }
    current=(TOKEN) lexer->yylex();
    cout << "WHILE" << tagNum << " :" << endl;
    type = Expression();
    if (type != BOOL)
    {
        Error("BOOL attendu");
    }
    cout <<"\tpop %rax" << endl;
    cout <<"\tcmpq $0, %rax" << endl;
    cout <<"\tjne DO" <<tagNum << endl;
    cout <<"FINDOWHILE" <<tagNum << " :" << endl;
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	int myTag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "BEGIN")==0)
	{
		current=(TOKEN) lexer->yylex();	
		Statement();
		
		if(strcmp(lexer->YYText(), ";")==0)
		{
			current=(TOKEN) lexer->yylex();	
			Statement();
		}
		
		if(current==KEYWORD && strcmp(lexer->YYText(), "END")==0)
		{
			current=(TOKEN) lexer->yylex();	
			cout << "\tjmp suite"<<myTag<<endl;
		}
		else{Error("END attendu");}
	}
	else{Error("BEGIN attendu");}
	
	cout << "suite"<<myTag<<":"<<endl;
}

void Display(void)
{
    TYPE type;
	int myTag = ++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	switch(type)
	{
		case INT:
			cout << "\tpop %rsi\t# The value to be displayed"<<endl;
			cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		case BOOL:
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<myTag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<myTag<<endl;
			cout << "False"<<myTag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<myTag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;
			break;
		case DOUBLE:
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
			break;
		case CHAR:
			cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;
		default:
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the Charlly's Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
