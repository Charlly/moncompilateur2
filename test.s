			# This code was produced by the Charlly's Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
b:	.double 0.0
z:	.double 0.0
a:	.quad 0
c:	.quad 0
i:	.quad 0
x:	.quad 0
ch:	.byte 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$0, (%rsp)	# Conversion of 2.5 (32 bit high part)
	movl	$1074003968, 4(%rsp)	# Conversion of 2.5 (32 bit low part)
	pop b
	subq $8,%rsp			# allocate 8 bytes on stack's top
	movl	$0, (%rsp)	# Conversion of 2 (32 bit high part)
	movl	$1073741824, 4(%rsp)	# Conversion of 2 (32 bit low part)
	pop z
	push $0
	pop i
	push $0
	push $1
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai4	# If below
	push $0		# False
	jmp Suite4
Vrai4:	push $0xFFFFFFFFFFFFFFFF		# True
Suite4:
	pop x
	movq $0, %rax
	movb $'y',%al
	push %rax	# push a 64-bit version of 'y'
	pop %rax
	movb %al,ch
	push $2
	pop a
for6:
	movq $1, %r8
	push a
	push $4
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jae Vrai8	# If above or equal
	push $0		# False
	jmp Suite8
Vrai8:	push $0xFFFFFFFFFFFFFFFF		# True
Suite8:
	pop %rbx
	movq $0, %rax
	cmpq %rax, %rbx
	ja suite6
	push $2
	pop %r8
	push b
	push z
	fldl	8(%rsp)	
	fldl	(%rsp)	# first operand -> %st(0) ; second operand -> %st(1)
	fmulp	%st(0),%st(1)	# %st(0) <- op1 + op2 ; %st(1)=null
	fstpl 8(%rsp)
	addq	$8, %rsp	# result on stack's top
	pop b
	addq %r8, a
	jmp for6
suite6:
	push b
	movsd	(%rsp), %xmm0		# &stack top -> %xmm0
	subq	$16, %rsp		# allocation for 3 additional doubles
	movsd %xmm0, 8(%rsp)
	movq $FormatString2, %rdi	# "%lf\n"
	movq	$1, %rax
	call	printf
nop
	addq $24, %rsp			# pop nothing
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push x
	pop %rdx	# Zero : False, non-zero : true
	cmpq $0, %rdx
	je False14
	movq $TrueString, %rdi	# "TRUE\n"
	jmp Next14
False14:
	movq $FalseString, %rdi	# "FALSE\n"
Next14:
	call	puts@PLT
	push ch
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
SWITCHCASE20 :
	push i
OF20 :
	pop %rcx
CASE21 :
	movq %rcx, %rax
	push $0
	pop %rbx
	cmpq %rax, %rbx
	jne CASE22
	push a
	push $5
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	jmp CASEEND20
CASE22 :
	movq %rcx, %rax
	push $1
	pop %rbx
	cmpq %rax, %rbx
	jne CASE24
	push a
	push $5
	pop %rbx
	pop %rax
	subq	%rbx, %rax	# SUB
	push %rax
	pop a
	jmp CASEEND20
CASE24 :
	movq %rcx, %rax
	push $2
	pop %rbx
	cmpq %rax, %rbx
	jne CASE26
	jmp WHILE28
DO28 :
	push a
	push $100
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
WHILE28 :
	push a
	push $1000
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai30	# If below or equal
	push $0		# False
	jmp Suite30
Vrai30:	push $0xFFFFFFFFFFFFFFFF		# True
Suite30:
	pop %rax
	cmpq $0, %rax
	jne DO28
FINDOWHILE28 :
	jmp suite27
suite27:
	jmp CASEEND20
CASE26 :
	movq %rcx, %rax
	push $3
	pop %rbx
	cmpq %rax, %rbx
	jne CASE31
	push i
	push $100
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop i
	jmp CASEEND20
CASE31 :
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
CASEEND20 :
	push a
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'\n',%al
	push %rax	# push a 64-bit version of '\n'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
